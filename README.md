# Online Class-Incremental Learning For Real-World Food Classification
Published at WACV 2024 

Paper available: [[Click here!]](https://openaccess.thecvf.com/content/WACV2024/papers/Raghavan_Online_Class-Incremental_Learning_for_Real-World_Food_Image_Classification_WACV_2024_paper.pdf)
## Requirements

![](https://img.shields.io/badge/python-3.7-green.svg)
![](https://img.shields.io/badge/torch-1.5.1-blue.svg)
![](https://img.shields.io/badge/torchvision-0.6.1-blue.svg)
![](https://img.shields.io/badge/PyYAML-5.3.1-blue.svg)
![](https://img.shields.io/badge/scikit--learn-0.23.0-blue.svg)
----
Create a virtual enviroment
```sh
virtualenv food-ocil
```
Activating a virtual environment
```sh
source food-ocil/bin/activate
```
Installing packages
```sh
pip install -r requirements.txt
```

## Datasets 

### Online Class Incremental Learning
- Food-101
- VFN (VIPER FoodNet)

### Data preparation
- Download Food-101 dataset from: [[here]](https://data.vision.ee.ethz.ch/cvl/datasets\_extra/food-101/)
- Download VFN dataset

## Algorithms 

* iCaRL: Incremental Classifier and Representation Learning (**CVPR, 2017**) [[Paper]](https://arxiv.org/abs/1611.07725)
* MIR: Maximally Interfered Retrieval (**NeurIPS, 2019**) [[Paper]](https://proceedings.neurips.cc/paper/2019/hash/15825aee15eb335cc13f9b559f166ee8-Abstract.html)
* GSS: Gradient-Based Sample Selection (**NeurIPS, 2019**) [[Paper]](https://arxiv.org/pdf/1903.08671.pdf)
* ASER: Online class- incremental continual learning with adversarial shapley value (**AAAI, 2021**) [[Paper]](https://arxiv.org/pdf/2009.00093.pdf)
* DVC: Not Just Selection, but Exploration: Online Class-Incremental Continual Learning via Dual View Consistency (**CVPR, 2022**) [[Paper]](https://openaccess.thecvf.com/content/CVPR2022/papers/Gu_Not_Just_Selection_but_Exploration_Online_Class-Incremental_Continual_Learning_via_CVPR_2022_paper.pdf)

## Run commands
Detailed descriptions of options can be found in [general\_main.py](general_main.py)

### Sample commands to run algorithms on food-101
```shell

#MIR
python general_main.py --data food101  --cl_type nc --num_tasks 5   --agent ER --retrieve MIR --update random --batch 16 --eps_mem_batch 16  --mem_size 2000  --fix_order False --randomize_seed True  --external_data True --seed 1200 --num_runs_val 1 --verbose False --learning_rate 0.001 --alpha_val 1  --dynamic_model_update True

#GSS
python general_main.py --data food101 --cl_type nc --num_tasks 5 --agent ER --retrieve random --update GSS --batch 16 --eps_mem_batch 16 --gss_mem_strength 20 --mem_size 2000 --external_data True --alpha_val 1 

#iCaRL
python general_main.py --data food101 --cl_type nc --num_tasks 10  --agent ICARL --retrieve random --update random --batch 16 --eps_mem_batch 16  --mem_size 2000  --fix_order False --randomize_seed False --seed 400 --external_data True --alpha_val 1 

#ASER
python general_main.py --data cifar100 --cl_type nc --agent ER --update ASER --retrieve ASER --mem_size 2000 --aser_type asvm --n_smp_cls 1.5 --k 3 --fix_order False --randomize_seed False --seed 2022 --external_data False --device 0 --num_tasks 10 --alpha_val 1  --batch 16
```

#### Note: 
"--alpha_val" controls the diet patterns: 1 = short term diet, 2 = moderate term diet and 3 = long term diet

## Repo Structure & Description
    ├──agents                       #Files for different algorithms
        ├──base.py                      #Abstract class for algorithms
        ├──exp_replay.py                #File for ER, MIR and GSS
        ├──iCaRL.py                     #File for iCaRL
        ├──scr.py                       #File for SCR
    
    ├──continuum                    #Files for create the data stream objects
        ├──dataset_scripts              #Files for processing each specific dataset
            ├──dataset_base.py              #Abstract class for dataset
            ├──cifar100.py                  #File for CIFAR100
            ├──food101.py                   #File for Food101
            ├──vfn.py.                      #File for VFN
            

        ├──continuum.py             
        ├──data_utils.py

    
    ├──models                       #Files for backbone models
        ├──pretrained.py                #Files for pre-trained models
        ├──resnet.py                    #Files for ResNet
    
    ├──utils                        #Files for utilities
        ├──buffer                       #Files related to buffer
            ├──aser_retrieve.py             #File for ASER retrieval
            ├──aser_update.py               #File for ASER update
            ├──aser_utils.py                #File for utilities for ASER
            ├──buffer.py                    #Abstract class for buffer
            ├──buffer_utils.py              #General utilities for all the buffer files
            ├──gss_greedy_update.py         #File for GSS update
            ├──mir_retrieve.py              #File for MIR retrieval
            ├──random_retrieve.py           #File for random retrieval
            ├──reservoir_update.py          #File for random update
    
        ├──global_vars.py               #Global variables for CN-DPM
        ├──io.py                        #Code related to load and store csv or yarml
        ├──kd_manager.py                #File for knowledge distillation
        ├──name_match.py                #Match name strings to objects 
        ├──setup_elements.py            #Set up and initialize basic elements
        ├──utils.py                     #File for general utilities
    
    ├──config                       #Config files for hyper-parameters tuning
        ├──agent                        #Config files related to agents
        ├──data                         #Config files related to dataset
    
        ├──general_*.yml                #General yml (fixed variables, not tuned)
        ├──global.yml                   #paths to store results 

## Citation

If you use this paper/code in your research, please consider citing us:

**Online Class-Incremental Learning For Real-World Food Classification**

[Published at WACV 2024](https://openaccess.thecvf.com/content/WACV2024/papers/Raghavan_Online_Class-Incremental_Learning_for_Real-World_Food_Image_Classification_WACV_2024_paper.pdf)

```
@InProceedings{Raghavan_2024_WACV,
    author    = {Raghavan, Siddeshwar and He, Jiangpeng and Zhu, Fengqing},
    title     = {Online Class-Incremental Learning for Real-World Food Image Classification},
    booktitle = {Proceedings of the IEEE/CVF Winter Conference on Applications of Computer Vision (WACV)},
    month     = {January},
    year      = {2024},
    pages     = {8195-8204}
}
```

## Contact
- [Siddeshwar Raghavan](https://siddeshwar-raghavan.github.io/)
- [Jiangpeng He](https://engineering.purdue.edu/people/jiangpeng.he.1) (Corresponding author)

## Note
Code is based on the this [implementation](https://github.com/RaptorMai/online-continual-learning)
