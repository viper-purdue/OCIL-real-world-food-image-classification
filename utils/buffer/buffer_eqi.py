from utils.setup_elements import input_size_match
from utils import name_match #import update_methods, retrieve_methods
from utils.utils import maybe_cuda
import torch
from utils.buffer.buffer_utils import BufferClassTracker
from utils.setup_elements import n_classes

import torch
from collections import defaultdict

class BufferEqi(torch.nn.Module):
    def __init__(self, model, params):
        super().__init__()
        self.params = params
        self.model = model
        self.cuda = self.params.cuda
        self.device = "cuda" if self.params.cuda else "cpu"
        
        # define buffer: dictionary-based class-wise storage
        self.buffer_img = defaultdict(list)
        self.buffer_label = defaultdict(list)
        self.max_samples_per_class = 20
        
        # define update and retrieve method
        self.update_method = name_match.update_methods[params.update](params)
        self.retrieve_method = name_match.retrieve_methods[params.retrieve](params)

        if self.params.buffer_tracker:
            self.buffer_tracker = BufferClassTracker(n_classes[params.data], self.device)

    def update(self, x, y, **kwargs):
        for xi, yi in zip(x, y):
            # Check if we have not exceeded the max samples per class
            if len(self.buffer_img[yi]) < self.max_samples_per_class:
                self.buffer_img[yi].append(xi)
                self.buffer_label[yi].append(yi)
            else:
                # Optionally implement strategy to replace or manage overflow
            
            # Update any other related attributes like self.n_seen_so_far
            # and self.current_index based on your requirements

        return self.update_method.update(buffer=self, x=x, y=y, **kwargs)

    def retrieve(self, **kwargs):
        return self.retrieve_method.retrieve(buffer=self, **kwargs)
