import torch
from torch.utils import data
from utils.buffer.buffer import Buffer
from agents.base import ContinualLearner
from continuum.data_utils import dataset_transform
from utils.setup_elements import transforms_match, input_size_match
from utils.utils import maybe_cuda, AverageMeter
from kornia.augmentation import RandomResizedCrop, RandomHorizontalFlip, ColorJitter, RandomGrayscale
import torch.nn as nn
import numpy as np
import pandas as pd

import copy

class SupContrastReplay(ContinualLearner):
    def __init__(self, model, opt, params):
        super(SupContrastReplay, self).__init__(model, opt, params)
        self.buffer = Buffer(model, params)
        self.mem_size = params.mem_size
        self.eps_mem_batch = params.eps_mem_batch
        self.mem_iters = params.mem_iters
        self.transform = nn.Sequential(
            RandomResizedCrop(size=(input_size_match[self.params.data][1], input_size_match[self.params.data][2]), scale=(0.2, 1.)),
            RandomHorizontalFlip(),
            ColorJitter(0.4, 0.4, 0.4, 0.1, p=0.8),
            RandomGrayscale(p=0.2)

        )
        self.dyn_model_update = params.dyn_model_update

    def train_learner(self, x_train, y_train):
        self.before_train(x_train, y_train)
        # set up loader
        train_dataset = dataset_transform(x_train, y_train, transform=transforms_match[self.data])
        train_loader = data.DataLoader(train_dataset, batch_size=self.batch, shuffle=True, num_workers=0,
                                       drop_last=True)
        # set up model
        self.model = self.model.train()

        # setup tracker
        losses = AverageMeter()
        acc_batch = AverageMeter()
        #virtual update trackers
        losses_vm1 = AverageMeter()
        losses_vm2 = AverageMeter()
        acc_batch_vm1 = AverageMeter()
        acc_batch_vm2 = AverageMeter()


        #setup labels
        overlapped_labels = list(set(self.new_labels).intersection(set(self.old_labels)))  #recalculate logits for these
        new_non_overlap_labels = list(set(self.new_labels).difference(set(overlapped_labels))) #non-overlapped to be updated
        cumm_new_labels = self.old_labels + new_non_overlap_labels

        for ep in range(self.epoch):
            for i, batch_data in enumerate(train_loader):
                # batch update
                batch_x, batch_y = batch_data
                batch_x = maybe_cuda(batch_x, self.cuda)
                batch_y = maybe_cuda(batch_y, self.cuda)
                batch_x_copy = batch_x.clone()
                batch_y_copy = batch_y.clone()
                #print('size of batch x and batch y: ', batch_x.size(), batch_y.size())

                for j in range(self.mem_iters):
                    if self.dyn_model_update:
                        vm1 = copy.deepcopy(self.model)
                        vm2 = copy.deepcopy(self.model)

                        #VM1 - full data
                        with torch.no_grad():
                            mem_x_vm1, mem_y_vm1 = self.buffer.retrieve(x=batch_x, y=batch_y)
                            if mem_x_vm1.size(0) > 0:
                                mem_x_vm1 = maybe_cuda(mem_x_vm1, self.cuda)
                                mem_y_vm1 = maybe_cuda(mem_y_vm1, self.cuda)
                                combined_batch_vm1 = torch.cat((mem_x_vm1, batch_x))
                                combined_labels_vm1 = torch.cat((mem_y_vm1, batch_y))
                                combined_batch_aug_vm1 = self.transform(combined_batch_vm1)
                                features_vm1 = torch.cat([vm1.forward(combined_batch_vm1).unsqueeze(1), vm1.forward(combined_batch_aug_vm1).unsqueeze(1)], dim=1)
                                loss_vm1 = self.criterion(features_vm1, combined_labels_vm1)
                                losses_vm1.update(loss_vm1, batch_y.size(0))

                                # print('len vm1 combined labels: ', len(combined_labels_vm1))
                                # _, pred_label_vm1 = torch.max(features_vm1, 1)
                                # print('vm1 pred labels: ', pred_label_vm1)
                                # correct_cnt_mem_vm1 = (pred_label_vm1 == combined_labels_vm1).sum().item() / combined_labels_vm1.size(0)
                                # print('correct cnt vm1: ',correct_cnt_mem_vm1)
                                
                                # acc_batch_vm1.update(correct_cnt_mem_vm1, combined_labels_vm1.size(0))
                                acc_vm1 = losses_vm1.avg()
                                print('avg losses vm1: ', acc_vm1)
                            else:
                                acc_vm1 = 0
                        
                        #VM2 - new data only
                        batch_x_vm2 = []
                        batch_y_vm2 = []
                        temp_y = batch_y.detach().cpu().numpy()
                        temp_x = batch_x.detach().cpu().numpy()
                        for i in range(len(temp_y)):
                            if temp_y[i] in new_non_overlap_labels:
                                batch_x_vm2.append(temp_x[i,:,:,:])
                                batch_y_vm2.append(temp_y[i])

                        batch_x_vm2 = torch.tensor(np.array(batch_x_vm2))
                        batch_y_vm2 = torch.tensor(np.array(batch_y_vm2))
                        batch_x_vm2 = maybe_cuda(batch_x_vm2, self.cuda)
                        batch_y_vm2 = maybe_cuda(batch_y_vm2, self.cuda)
                        with torch.no_grad():
                            mem_x_vm2, mem_y_vm2 = self.buffer.retrieve(x=batch_x_vm2, y=batch_y_vm2)
                            if mem_x_vm2.size(0) > 0:
                                mem_x_vm2 = maybe_cuda(mem_x_vm2, self.cuda)
                                mem_y_vm2 = maybe_cuda(mem_y_vm2, self.cuda)
                                combined_batch_vm2 = torch.cat((mem_x_vm2, batch_x_vm2))
                                combined_labels_vm2 = torch.cat((mem_y_vm2, batch_y_vm2))
                                combined_batch_aug_vm2 = self.transform(combined_batch_vm2)
                                features_vm2 = torch.cat([vm2.forward(combined_batch_vm2).unsqueeze(1), vm1.forward(combined_batch_aug_vm2).unsqueeze(1)], dim=1)
                                loss_vm2 = self.criterion(features_vm2, combined_labels_vm2)
                                # correct_cnt_mem_vm2 = (features_vm2 == combined_labels_vm2).sum().item() / combined_labels_vm2.size(0)
                                losses_vm2.update(loss_vm2, batch_y_vm2.size(0))
                                # acc_batch_vm2.update(correct_cnt_mem_vm2, combined_labels_vm2.size(0))
                                acc_vm2 = losses_vm2.avg()
                                print('avg losses vm2: ', acc_vm2)
                            else:
                                acc_vm2 = 0
                        
                        if acc_vm2 > acc_vm1 and acc_vm2 > 0.55:
                            batch_x = batch_x_vm2
                            batch_y = batch_y_vm2
                        elif acc_vm1 > acc_vm2 and acc_vm1 > 0.55:
                            batch_x = batch_x_copy
                            batch_y = batch_y_copy

                    ####################################################################################################################        
                    mem_x, mem_y = self.buffer.retrieve(x=batch_x, y=batch_y)

                    if mem_x.size(0) > 0:
                        mem_x = maybe_cuda(mem_x, self.cuda)
                        mem_y = maybe_cuda(mem_y, self.cuda)
                        #print('size of mem x and mem y: ', mem_x.size(), mem_y.size())
                        combined_batch = torch.cat((mem_x, batch_x))
                        combined_labels = torch.cat((mem_y, batch_y))
                        combined_batch_aug = self.transform(combined_batch)
                        features = torch.cat([self.model.forward(combined_batch).unsqueeze(1), self.model.forward(combined_batch_aug).unsqueeze(1)], dim=1)
                        #print('features op: ', features.shape)
                        loss = self.criterion(features, combined_labels)
                        #correct_cnt = (features == combined_labels).sum().item() / combined_labels.size(0)
                        losses.update(loss, batch_y.size(0))
                        #acc_batch.update(correct_cnt, combined_labels.size(0))
                        self.opt.zero_grad()
                        loss.backward()
                        self.opt.step()

                # update mem
                self.buffer.update(batch_x, batch_y)
                if i % 100 == 1 and self.verbose:
                        print(
                            '==>>> it: {}, avg. loss: {:.6f}, '
                                .format(i, losses.avg(), acc_batch.avg())
                        )
        self.after_train()
