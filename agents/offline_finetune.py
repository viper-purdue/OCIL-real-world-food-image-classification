import torch
from torch.utils import data
from utils.buffer.buffer import Buffer
from agents.base import ContinualLearner
from continuum.data_utils import dataset_transform
from utils.setup_elements import transforms_match
from utils.utils import maybe_cuda, AverageMeter, SumMeter
import pandas as pd
import copy
import numpy as np
import time
import json


class OfflineFinetune(ContinualLearner):
    def __init__(self, model, opt, params):
        super(OfflineFinetune, self).__init__(model, opt, params)
        self.buffer = Buffer(model, params)
        # self.mem_size = params.mem_size
        # self.eps_mem_batch = params.eps_mem_batch
        self.mem_iters = params.mem_iters
        # self.dyn_model_update = params.dyn_model_update
        # self.dyn_buffer_update = params.dyn_buffer_update

    def train_learner(self, x_train, y_train):
        self.before_train(x_train, y_train)
        # set up loader
        train_dataset = dataset_transform(x_train, y_train, transform=transforms_match[self.data])
        train_loader = data.DataLoader(train_dataset, batch_size=self.batch, shuffle=True, num_workers=0,
                                       drop_last=True)
        # set up model
        self.model = self.model.train()


	#setup labels
        overlapped_labels = list(set(self.new_labels).intersection(set(self.old_labels)))  #recalculate logits for these
        new_non_overlap_labels = list(set(self.new_labels).difference(set(overlapped_labels))) #non-overlapped to be updated
        cumm_new_labels = self.old_labels + new_non_overlap_labels

        for ep in range(self.epoch):
            total_time_start = time.time()
            for i, batch_data in enumerate(train_loader):
                # batch update
                batch_x, batch_y = batch_data
                batch_x = maybe_cuda(batch_x, self.cuda)
                batch_y = maybe_cuda(batch_y, self.cuda)
                #zero the gradients
                self.opt.zero_grad()
                logits = self.model.forward(batch_x)
                loss = self.criterion(logits, batch_y)
                _, pred_label = torch.max(logits, 1)
                correct_cnt = (pred_label == batch_y).sum().item() / batch_y.size(0)
                # backward
                loss.backward()
                self.opt.step()

        self.after_train()


