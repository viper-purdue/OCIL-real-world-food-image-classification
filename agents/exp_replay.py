import torch
from torch.utils import data
from utils.buffer.buffer import Buffer
from agents.base import ContinualLearner
from continuum.data_utils import dataset_transform
from utils.setup_elements import transforms_match
from utils.utils import maybe_cuda, AverageMeter, SumMeter
import pandas as pd
import copy
import numpy as np
import time
import json


class ExperienceReplay(ContinualLearner):
    def __init__(self, model, opt, params):
        super(ExperienceReplay, self).__init__(model, opt, params)
        self.buffer = Buffer(model, params)
        self.mem_size = params.mem_size
        self.eps_mem_batch = params.eps_mem_batch
        self.mem_iters = params.mem_iters
        self.dyn_model_update = params.dyn_model_update
        self.dyn_buffer_update = params.dyn_buffer_update

    def train_learner(self, x_train, y_train):
        self.before_train(x_train, y_train)
        # set up loader
        train_dataset = dataset_transform(x_train, y_train, transform=transforms_match[self.data])
        train_loader = data.DataLoader(train_dataset, batch_size=self.batch, shuffle=True, num_workers=0,
                                       drop_last=True)
        # set up model
        self.model = self.model.train()

        # setup tracker
        losses_batch = AverageMeter()
        losses_mem = AverageMeter()
        acc_batch = AverageMeter()
        acc_mem = AverageMeter()
        #virtual update trackers
        acc_batch_vm1 = AverageMeter()
        acc_mem_vm1 = AverageMeter()
        acc_batch_vm2 = AverageMeter()
        acc_mem_vm2 = AverageMeter()
        time_vm1 = SumMeter()
        time_vm2 = SumMeter()

	#setup labels
        overlapped_labels = list(set(self.new_labels).intersection(set(self.old_labels)))  #recalculate logits for these
        new_non_overlap_labels = list(set(self.new_labels).difference(set(overlapped_labels))) #non-overlapped to be updated
        cumm_new_labels = self.old_labels + new_non_overlap_labels

        for ep in range(self.epoch):
            total_time_start = time.time()
            for i, batch_data in enumerate(train_loader):
                # batch update
                batch_x, batch_y = batch_data
                batch_x = maybe_cuda(batch_x, self.cuda)
                batch_y = maybe_cuda(batch_y, self.cuda)
                batch_x_copy = batch_x.clone()
                batch_y_copy = batch_y.clone()
                for j in range(self.mem_iters):

                    ########################################
                    #VM1 - full batch
                    if self.dyn_model_update and self.task_seen > 0:
                        vm1 = copy.deepcopy(self.model)
                        vm2 = copy.deepcopy(self.model)
                        with torch.no_grad():
                            
                            if batch_x.ndim == 4:
                                t1_start = time.time()
                                logits_vm1 = vm1.forward(batch_x)
                                loss_vm1 = self.criterion(logits_vm1, batch_y)
                                _, pred_label_vm1 = torch.max(logits_vm1, 1)
                                correct_cnt_vm1 = (pred_label_vm1 == batch_y).sum().item() / batch_y.size(0)
                                # update virtual tracker
                                acc_batch_vm1.update(correct_cnt_vm1, batch_y.size(0))
                                
                                mem_x_vm1, mem_y_vm1 = self.buffer.retrieve(x=batch_x, y=batch_y)
                                if mem_x_vm1.size(0) > 0:
                                    mem_x_vm1 = maybe_cuda(mem_x_vm1, self.cuda)
                                    mem_y_vm1 = maybe_cuda(mem_y_vm1, self.cuda)
                                    mem_logits_vm1 = vm1.forward(mem_x_vm1)
                                    _, pred_label_mem_vm1 = torch.max(mem_logits_vm1, 1)
                                    correct_cnt_mem_vm1 = (pred_label_mem_vm1 == mem_y_vm1).sum().item() / mem_y_vm1.size(0)
                                    acc_mem_vm1.update(correct_cnt_mem_vm1, mem_y_vm1.size(0))

                                    acc_vm1 = (acc_batch_vm1.avg() + acc_mem_vm1.avg()) / 2
                                else:
                                    acc_vm1 = acc_batch_vm1.avg()
                                t1_end = time.time()
                                time_vm1.update(t1_end-t1_start, 1)
                            else:
                                acc_vm1 = 0

                        

                        #VM2 - new data only
                        batch_x_vm2 =  []
                        batch_y_vm2 = []
                        tmp_y = batch_y.detach().cpu().numpy()
                        tmp_x = batch_x.detach().cpu().numpy()
                        for i in range(len(tmp_y)):
                            if tmp_y[i] in new_non_overlap_labels:
                                batch_x_vm2.append(tmp_x[i, :, :, :])
                                batch_y_vm2.append(tmp_y[i])
                                
                        batch_x_vm2 = torch.tensor(np.array(batch_x_vm2))
                        batch_y_vm2 = torch.tensor(np.array(batch_y_vm2))
                        batch_x_vm2 = maybe_cuda(batch_x_vm2, self.cuda)
                        batch_y_vm2 = maybe_cuda(batch_y_vm2, self.cuda)
                        with torch.no_grad():
                            if batch_x_vm2.ndim == 4:
                                t2_start = time.time()
                                logits_vm2 = vm2.forward(batch_x_vm2)
                                loss_vm2 = self.criterion(logits_vm2, batch_y_vm2)
                                _, pred_label_vm2 = torch.max(logits_vm2, 1)
                                correct_cnt_vm2 = (pred_label_vm2 == batch_y_vm2).sum().item() / batch_y_vm2.size(0)
                                # update virtual tracker
                                acc_batch_vm2.update(correct_cnt_vm2, batch_y_vm2.size(0))
                                
                                mem_x_vm2, mem_y_vm2 = self.buffer.retrieve(x=batch_x, y=batch_y)
                                if mem_x_vm2.size(0) > 0:
                                    mem_x_vm2 = maybe_cuda(mem_x_vm2, self.cuda)
                                    mem_y_vm2 = maybe_cuda(mem_y_vm2, self.cuda)
                                    mem_logits_vm2 = vm2.forward(mem_x_vm2)
                                    _, pred_label_mem_vm2 = torch.max(mem_logits_vm2, 1)
                                    correct_cnt_mem_vm2 = (pred_label_mem_vm2 == mem_y_vm2).sum().item() / mem_y_vm2.size(0)
                                    acc_mem_vm2.update(correct_cnt_mem_vm2, mem_y_vm2.size(0))

                                    acc_vm2 = (acc_batch_vm2.avg() + acc_mem_vm2.avg()) / 2
                                else:
                                    acc_vm2 = acc_batch_vm2.avg()
                                t2_end = time.time()
                                time_vm2.update(t2_end-t2_start, 1)
                            else:
                                acc_vm2 = 0
                        
                        # print('self.acc_compare: ',self.acc_compare)
                        if acc_vm2 > acc_vm1 and acc_vm2 > (self.acc_compare - (self.acc_compare/4)): 
                            #print('batch avg: ', acc_batch.avg(),' mem avg:  ', acc_mem.avg())
                            batch_x = batch_x_vm2
                            batch_y = batch_y_vm2
                            mem_x = mem_x_vm2
                            mem_y = mem_y_vm2

                        elif acc_vm1 > acc_vm2 and acc_vm1 > (self.acc_compare - (self.acc_compare/4)): 
                            batch_x = batch_x_copy
                            batch_y = batch_y_copy
                            mem_x = mem_x_vm1
                            mem_y = mem_y_vm1
                        else:
                            mem_x = mem_x_vm1
                            mem_y = mem_y_vm1

                    ####################################################    

                    if batch_x.ndim == 4:
                        logits = self.model.forward(batch_x)
                        loss = self.criterion(logits, batch_y)
                        _, pred_label = torch.max(logits, 1)
                        correct_cnt = (pred_label == batch_y).sum().item() / batch_y.size(0)
                        # update tracker
                        acc_batch.update(correct_cnt, batch_y.size(0))
                        losses_batch.update(loss, batch_y.size(0))
                        # backward
                        self.opt.zero_grad()
                        loss.backward()

                        if self.dyn_model_update:
                            # mem update
                            batch_x = batch_x_copy
                            batch_y = batch_y_copy

                        if not self.dyn_model_update or self.task_seen==0:
                            mem_x, mem_y = self.buffer.retrieve(x=batch_x, y=batch_y)
                        if mem_x.size(0) > 0:
                            mem_x = maybe_cuda(mem_x, self.cuda)
                            mem_y = maybe_cuda(mem_y, self.cuda)
                            mem_logits = self.model.forward(mem_x)
                            loss_mem = self.criterion(mem_logits, mem_y)
                            # update tracker
                            losses_mem.update(loss_mem, mem_y.size(0))
                            _, pred_label = torch.max(mem_logits, 1)
                            correct_cnt = (pred_label == mem_y).sum().item() / mem_y.size(0)
                            acc_mem.update(correct_cnt, mem_y.size(0))

                            score_mem = correct_cnt / mem_y.size(0)
                            #print('score me: ', score_mem)

                            loss_mem.backward()

                        if self.params.update == 'ASER' or self.params.retrieve == 'ASER':
                            # opt update
                            self.opt.zero_grad()
                            combined_batch = torch.cat((mem_x, batch_x))
                            combined_labels = torch.cat((mem_y, batch_y))
                            combined_logits = self.model.forward(combined_batch)
                            loss_combined = self.criterion(combined_logits, combined_labels)
                            loss_combined.backward()
                            self.opt.step()
                        else:
                            self.opt.step()


                #Buffer update
                self.buffer.update(batch_x, batch_y)

                if i % 100 == 1 and self.verbose:
                    print(
                        '==>>> it: {}, avg. loss: {:.6f}, '
                        'running train acc: {:.3f}'
                            .format(i, losses_batch.avg(), acc_batch.avg())
                    )
                    print(
                        '==>>> it: {}, mem avg. loss: {:.6f}, '
                        'running mem acc: {:.3f}'
                            .format(i, losses_mem.avg(), acc_mem.avg())
                    )
            total_time_end = time.time()
        self.after_train()


