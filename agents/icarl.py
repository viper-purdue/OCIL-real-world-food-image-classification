from agents.base import ContinualLearner
from continuum.data_utils import dataset_transform
from utils import utils
from utils.buffer.buffer_utils import random_retrieve
from utils.setup_elements import transforms_match
from torch.utils import data
import numpy as np
from torch.nn import functional as F
from utils.utils import maybe_cuda, AverageMeter
from utils.buffer.buffer import Buffer
import torch
import copy
import pandas as pd
from sklearn.metrics import top_k_accuracy_score
from torchmetrics.functional import precision_recall
from torchmetrics import F1Score


class Icarl(ContinualLearner):
    def __init__(self, model, opt, params):
        super(Icarl, self).__init__(model, opt, params)
        self.model = model
        self.mem_size = params.mem_size
        self.buffer = Buffer(model, params)
        self.prev_model = None
        self.dyn_model_update = params.dyn_model_update
        self.dyn_buffer_update = params.dyn_buffer_update

    def train_learner(self, x_train, y_train):
        self.before_train(x_train, y_train)
        # set up loader
        train_dataset = dataset_transform(x_train, y_train, transform=transforms_match[self.data])
        train_loader = data.DataLoader(train_dataset, batch_size=self.batch, shuffle=True, num_workers=0,
                                       drop_last=True)
        self.model.train()
        self.update_representation(train_loader)
        self.prev_model = copy.deepcopy(self.model)
        self.after_train()

    def update_representation(self, train_loader):
        updated_idx = []

        acc_batch = AverageMeter()
        vm1_acc_batch = AverageMeter()
        vm2_acc_batch = AverageMeter()
        # vm1_prec_batch = AverageMeter()
        # vm1_rec_batch = AverageMeter()
        # vm2_prec_batch = AverageMeter()
        # vm2_rec_batch = AverageMeter()
        vm1_f1_batch = AverageMeter()
        vm2_f1_batch = AverageMeter()
        new_sample_flag = False


        for ep in range(self.epoch):
            for i, train_data in enumerate(train_loader):
                # batch update
                train_x, train_y = train_data
                #print('train y: ', train_y)
                train_x = maybe_cuda(train_x, self.cuda)
                train_y = maybe_cuda(train_y, self.cuda)
                train_x_copy1 = train_x.detach().clone()
                train_y_copy1 = train_y.detach().clone()
                train_x_copy2 = train_x.detach().clone()
                train_y_copy2 = train_y.detach().clone()
                train_y_copy = train_y.detach().clone()
                
                overlapped_labels = list(set(self.new_labels).intersection(set(self.old_labels)))  #recalculate logits for these
                new_non_overlap_labels = list(set(self.new_labels).difference(set(overlapped_labels))) #non-overlapped to be updated
                cumm_new_labels = self.old_labels + new_non_overlap_labels

                

                #################################################
                #Virtual model update
                if self.dyn_model_update:
                    if self.prev_model is not None:
                        vm1 = copy.deepcopy(self.model)
                        vm2 = copy.deepcopy(self.model)
                        
                        #VM2 - train for entire batch               
                        for k, y in enumerate(train_y_copy2):
                            train_y_copy2[k] = cumm_new_labels.index(y)
                        all_cls_num_vm2 = len(cumm_new_labels)
                        target_labels_vm2 = utils.ohe_label(train_y_copy2, all_cls_num_vm2, device = train_y_copy2.device).float()
                        mem_x_vm2, mem_y_vm2 = random_retrieve(self.buffer, self.batch, excl_indices=updated_idx)
                        mem_x_vm2 = maybe_cuda(mem_x_vm2, self.cuda)
                        mem_y_vm2 = maybe_cuda(mem_y_vm2, self.cuda)
                        batch_x_vm2 = torch.cat([train_x_copy2, mem_x_vm2])
                        target_labels_vm2 = torch.cat([target_labels_vm2, torch.zeros_like(target_labels_vm2)])
                        with torch.no_grad():
                            logits_vm2 = vm2.forward(batch_x_vm2)
                            q = torch.sigmoid(self.prev_model.forward(batch_x_vm2))
                            for k, y in enumerate(self.old_labels):
                                target_labels_vm2[:, k] = q[:, k]
                            _, pred_label_vm2 = torch.max(logits_vm2[:, :all_cls_num_vm2], 1)
                            _, true_label_vm2 = torch.max(target_labels_vm2,1)
                            #pred_label_vm2 = torch.sigmoid(logits_vm2[:, :all_cls_num_vm2])
                            vm2_correct_cnt = (pred_label_vm2 == true_label_vm2).sum().item()/true_label_vm2.size(0)
                            vm2_acc_batch.update(vm2_correct_cnt, true_label_vm2.size(0))
                            rec_recall_temp = precision_recall(pred_label_vm2, true_label_vm2,  average='micro')
                            f1_temp = (2 * rec_recall_temp[0] * rec_recall_temp[1]) / (rec_recall_temp[0] + rec_recall_temp[1])
                            vm2_f1_batch.update(f1_temp, true_label_vm2.size(0))



                        #VM1 - train for new labels only
                        train_x_vm1 = []
                        train_y_vm1 = []
                        train_y_copy1 = train_y_copy1.detach().cpu().numpy()
                        train_x_copy1 = train_x_copy1.detach().cpu().numpy()
                        for i in range(len(train_y_copy1)):  #create the non overlap  set in batch
                            if train_y_copy1[i] in new_non_overlap_labels:
                                train_x_vm1.append(train_x_copy1[i, :, :, :]) 
                                train_y_vm1.append(train_y_copy1[i])
                        train_x_vm1 = torch.tensor(np.array(train_x_vm1))
                        train_y_vm1 = torch.tensor(np.array(train_y_vm1))
                        train_x_vm1 = maybe_cuda(train_x_vm1, self.cuda)
                        train_y_vm1 = maybe_cuda(train_y_vm1, self.cuda)
                        train_y_vm1_copy = train_y_vm1.detach().clone()
                        # train_y_vm1_copy = train_y_vm1_copy.
                        
                        for k, y in enumerate(train_y_vm1):
                            train_y_vm1[k] = cumm_new_labels.index(y)
                        all_cls_num_vm1 = len(cumm_new_labels)
                        if train_y_vm1.size(0) != 0:
                            target_labels_vm1 = utils.ohe_label(train_y_vm1, all_cls_num_vm1, device = train_y_vm1.device).float()
                            mem_x_vm1, mem_y_vm1 = random_retrieve(self.buffer, train_y_vm1.size(0), excl_indices=updated_idx)
                            mem_x_vm1 = maybe_cuda(mem_x_vm1, self.cuda)
                            mem_y_vm1 = maybe_cuda(mem_y_vm1, self.cuda)
                            for k, y in enumerate(mem_y_vm1):
                                mem_y_vm1[k] = cumm_new_labels.index(y)

                            temp_forzeros = utils.ohe_label(mem_y_vm1, all_cls_num_vm1, device=mem_y_vm1.device).float()
                            batch_x_vm1 = torch.cat([train_x_vm1, mem_x_vm1])
                            target_labels_vm1 = torch.cat([target_labels_vm1, torch.zeros_like(temp_forzeros)])
                            with torch.no_grad():
                                logits_vm1 = vm1.forward(batch_x_vm1)
                                q = torch.sigmoid(self.prev_model.forward(batch_x_vm1))
                                for k, y in enumerate(self.old_labels):
                                    target_labels_vm1[:, k] = q[:, k]
                                _, pred_label_vm1 = torch.max(logits_vm1[:, :all_cls_num_vm1], 1)
                                _, true_label_vm1 = torch.max(target_labels_vm1,1)
                                #pred_label_vm1 = torch.sigmoid(logits_vm1[:, :all_cls_num_vm1])

                                vm1_correct_cnt = (pred_label_vm1 == true_label_vm1).sum().item()/true_label_vm1.size(0)
                                vm1_acc_batch.update(vm1_correct_cnt, true_label_vm1.size(0))
                                #f1_temp = F1Score(pred_label_vm1, true_label_vm1, average='micro')
                                rec_recall_temp = precision_recall(pred_label_vm1, true_label_vm1, average='micro')
                                f1_temp = (2 * rec_recall_temp[0] * rec_recall_temp[1]) / (rec_recall_temp[0] + rec_recall_temp[1])
                                vm1_f1_batch.update(f1_temp, true_label_vm1.size(0))
                        else:
                            vm1_f1_batch.update(0,0)


                        # if vm1_acc_batch.avg() > vm2_acc_batch.avg():
                        #     if vm1_acc_batch.avg() > 0.60:
                        #         train_x = train_x_vm1

                        # elif vm2_acc_batch.avg() > vm1_acc_batch.avg():
                        #     if vm2_acc_batch.avg() > 0.60:
                        #         train_x = train_x_copy2
                        #print('f1 avg vm2 and vm1: ', vm2_f1_batch.avg(), vm1_f1_batch.avg())
                        #if vm1_f1_batch.avg() > vm2_f1_batch.avg():
                        if vm1_f1_batch.avg()>vm2_f1_batch.avg() and vm1_f1_batch.avg() > (0.75*self.acc_compare) and len(train_x_vm1) > 0:
                            new_sample_flag = True
                            # print('only new samples and vm1 avg: ', vm1_f1_batch.avg())
                            train_x = train_x_vm1.detach().clone()
                            train_y_copy = train_y_vm1_copy.detach().clone()
                            train_y = train_y_vm1_copy.detach().clone()
                            # print('train vm1 copy: ', train_x.shape)
                            # mem_x = mem_x_vm1
                            # mem_y = mem_y_vm1
                                
                        #elif vm2_f1_batch.avg() > vm1_f1_batch.avg():
                        else: #vm2_f1_batch.avg() > 0.50:
                            new_sample_flag = False
                            train_x = train_x_copy2




                        #with torch.no_grad():

                            # vm1_features = vm1.features(train_x)
                            # vm1_logits = vm1.logits(vm1_features)
                            # q = torch.sigmoid(self.prev_model.forward(train_x))
                            # for k, y in enumerate(self.old_labels):
                            #     target_labels[:, k] = q[:, k]
                            # virtual_loss = F.binary_cross_entropy_with_logits(vm1_logits[:, :all_cls_num], target_labels, reduction='none').sum(dim=1).mean()
                            # _, virtual_pred_label = torch.max(vm1_logits[:, :all_cls_num], 1)
                            # _, virtual_true_label = torch.max(target_labels,1)
                            # print('predicted labels: ', virtual_pred_label, 'max of target label: ', virtual_true_label, 'train_y: ', train_y)
                            # virtual_correct_cnt = (virtual_pred_label == virtual_true_label).sum().item()/virtual_true_label.size(0)
                            # virtual_acc_batch.update(virtual_correct_cnt, virtual_true_label.size(0))
                        
                #################################################

                if self.prev_model is None: #or self.dyn_model_update == False:
                    #print('firas run train y copy: ', train_y_copy, ' train y : ', train_y,)
                    for k, y in enumerate(train_y_copy):
                        #print('prev model is none')
                        #print(' old labels: ', self.old_labels, ' train y copy: ', train_y_copy)
                        train_y_copy[k] = cumm_new_labels.index(y)
                    all_cls_num = len(cumm_new_labels)
                    target_labels = utils.ohe_label(train_y_copy, all_cls_num, device=train_y_copy.device).float()


                if self.prev_model is not None:
                    # print('prev model not none')
                    #print('train y cope: ', train_y_copy)
                    #print(' second train y copy: ', train_y_copy, ' train y : ', train_y)
                    for k, y in enumerate(train_y_copy):
                        train_y_copy[k] = cumm_new_labels.index(y)
                    all_cls_num = len(cumm_new_labels)
                    target_labels = utils.ohe_label(train_y_copy, all_cls_num, device=train_y_copy.device).float()
                    # print('train x shape and size: ', train_x.shape, len(train_x))
                    if len(train_x) == self.batch:
                        mem_x, mem_y = random_retrieve(self.buffer, self.batch, excl_indices=updated_idx)
                    elif len(train_x) < self.batch:
                        mem_x, mem_y = random_retrieve(self.buffer, len(train_x), excl_indices=updated_idx)
                    else:
                        print("ERROR - train_x cant be bigger than batch.")
                    mem_x = maybe_cuda(mem_x, self.cuda)
                    mem_y = maybe_cuda(mem_y, self.cuda)
                    batch_x = torch.cat([train_x, mem_x])
                    if new_sample_flag:
                        if train_y_copy.shape[0] == mem_y.shape[0]:
                            target_labels = torch.cat([target_labels, torch.zeros_like(target_labels)])
                        elif train_y_copy.shape[0] > mem_y.shape[0]:
                            target_labels_temp = utils.ohe_label(train_y_copy[:mem_y.shape[0]], all_cls_num, device=train_y_copy.device).float()
                            target_labels = torch.cat([target_labels, torch.zeros_like(target_labels_temp)])
                        elif train_y_copy.shape[0] < mem_y.shape[0]:
                            tmp_copy = train_y_copy
                            for i in range(abs(train_y_copy.shape[0] - mem_y.shape[0])):
                                tmp_copy = torch.cat((maybe_cuda(tmp_copy, self.cuda), maybe_cuda(torch.tensor([tmp_copy[-1]]), self.cuda)),0)

                            target_labels_temp = utils.ohe_label(tmp_copy, all_cls_num, device=tmp_copy.device).float()
                            target_labels = torch.cat([target_labels, torch.zeros_like(target_labels_temp)])

                    else:
                        target_labels = torch.cat([target_labels, torch.zeros_like(target_labels)])

                    score_target = torch.cat([train_y, mem_y])
                    score_target = score_target.detach().cpu().numpy()

                    #print('print train y score: ', train_y)
                    
                else:
                    batch_x = train_x

                    score_target = train_y.detach().cpu().numpy()

                logits = self.model.forward(batch_x)
                self.opt.zero_grad()
                if self.prev_model is not None:
                    with torch.no_grad():
                        q = torch.sigmoid(self.prev_model.forward(batch_x))
                    for k, y in enumerate(self.old_labels):
                        #print('train_x, target label, train_y_copy and q size: ', train_x.shape, target_labels.shape, train_y_copy.shape, q.shape)
                        #print('mem x and y size: ', mem_x.shape, mem_y.shape)
                        target_labels[:, k] = q[:, k]
                loss = F.binary_cross_entropy_with_logits(logits[:, :all_cls_num], target_labels, reduction='none').sum(dim=1).mean()
                loss.backward()
                self.opt.step()

                #calculate score for dynamic update of buffer 
                updated_idx += self.buffer.update(train_x, train_y)



