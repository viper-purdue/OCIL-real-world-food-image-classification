import numpy as np
from torchvision import datasets
from continuum.data_utils import create_task_composition, load_task_with_labels, create_task_composition_vfn_fixed, load_task_with_labels_vfn
from continuum.dataset_scripts.dataset_base import DatasetBase
from continuum.non_stationary import construct_ns_multiple_wrapper, test_ns
from collections import Counter

import os
import json
import torch
import cv2

from torch.utils.data import Dataset
import json
import random
from PIL import Image

class VFN(DatasetBase):
    def _get_img_from_paths(self, text_file):
        img_data = []
        img_data_paths = []
        data = []
        labels = []

        with open(text_file,'rb') as f:
            for line in f:
                temp = line.strip().decode("utf-8")
                temp = '../../VFN/'+temp[2:]
                data.append(temp.split('==')[0])
                labels.append(temp.split('==')[1])

        for datapath in data:
            # img = cv2.imread(datapath)
            # img_data.append(img)
            img_data_paths.append(datapath)
        # img_data = np.array(img_data)
        labels = np.array(list(map(int, labels)))
        print(text_file, len(img_data_paths), len(labels))
        return img_data_paths, labels

    def __init__(self, scenario, params):
        dataset = 'vfn'
        num_tasks = params.num_tasks
        self.train_file = '../../vfn_train.txt'
        self.test_file = '../../vfn_test.txt'
        super(VFN, self).__init__(dataset, scenario, num_tasks, params.num_runs, params)

    def download_load(self):
        self.train_data, self.train_label = self._get_img_from_paths(self.train_file)
        self.test_data, self.test_label = self._get_img_from_paths(self.test_file)

    def setup(self):
        if self.scenario == 'ni':
            self.train_set, self.val_set, self.test_set = construct_ns_multiple_wrapper(self.train_data,
                                                                                        self.train_label,
                                                                                        self.test_data, self.test_label,
                                                                                        self.task_nums, 32,
                                                                                        self.params.val_size,
                                                                                        self.params.ns_type, self.params.ns_factor,
                                                                                        plot=self.params.plot_sample)
        elif self.scenario == 'nc':
            self.task_labels, self.cumm_tasks_overlap_details = create_task_composition_vfn_fixed(class_nums=74, num_tasks=self.task_nums, alpha=self.params.alpha_val, beta=self.params.beta, distribution=self.params.distribution)
            result = []
            temp = []

            self.test_set = []
            for labels in self.task_labels:
                x_test, y_test = load_task_with_labels_vfn(self.test_data, self.test_label, labels, curr_task_details=None, cumm_overlap_details=None)
                self.test_set.append((x_test, y_test))
        else:
            raise Exception('wrong scenario')

    def new_task(self, cur_task, **kwargs):
        if self.scenario == 'ni':
            x_train, y_train = self.train_set[cur_task]
            labels = set(y_train)
        elif self.scenario == 'nc':
	    
            print('cur task: ', cur_task)
            labels = self.task_labels[cur_task]
            x_train, y_train = load_task_with_labels_vfn(self.train_data, self.train_label, labels, curr_task_details=self.cumm_tasks_overlap_details[cur_task], cumm_overlap_details=self.cumm_tasks_overlap_details[self.task_nums-1])
        return x_train, y_train, labels

    def new_run(self, **kwargs):
        self.setup()
        return self.test_set

    def test_plot(self):
        test_ns(self.train_data[:10], self.train_label[:10], self.params.ns_type, self.params.ns_factor)
        


