import numpy as np
from torchvision import datasets
from continuum.data_utils import create_task_composition, create_task_composition_non_overlap,  load_task_with_labels, shuffle_data, load_task_with_labels_baseline, create_task_composition_fixed
from continuum.dataset_scripts.dataset_base import DatasetBase
from continuum.non_stationary import construct_ns_multiple_wrapper, test_ns

import os
import json
import torch
import numpy as np
import cv2

from torch.utils.data import Dataset
import json
import random
from PIL import Image

total_classes = 101

#Generic dataloader
def unjson(file):
#    print('current path: ', os.getcwd())
    with open(file) as json_file:
        data = json.load(json_file)
        #print('train data size: ', len(x) )
    return data

def analyser_graph(labels, curr_phase_overlap, total_overlap):
    store_val_input = np.zeros((101,), dtype=int)
    for i in range(len(curr_phase_overlap)):
        if curr_phase_overlap[i] == 1 and total_overlap[i] == 1:
            store_val_input[i] = 600
        elif curr_phase_overlap[i] == 0 and total_overlap[i] == 0:
            store_val_input[i] = 0
        elif curr_phase_overlap[i] >= 1 and total_overlap[i] > 1:
            num_splits = 600 // total_overlap[i]
            store_val_input[i] = num_splits
        else:
            store_val_input[i] = 0
            
    #f = open('ER_input_mod.txt', 'a')
    #f.write(str(store_val_input.tolist()))
    #f.write('\n')
    #f.close()
        
    return store_val_input

class FOOD101(DatasetBase):
    def _load_img_from_array(self, idx):
    
        img = Image.fromarray(np.uint8(self.phase_dataset_data[idx])).convert('RGB')        
        return img

    def _get_img_from_path(self, data_dict):
        img_data = []
        for paths in data_dict:
            # print('paths: ', paths)
            if os.path.exists(paths):
                img_data.append(paths)
                # img = cv2.imread(paths)
                # if len(img.shape) == 3:
                #     # img = cv2.merge((img, img, img))
                #     
                # #print('shape of img before stack: ', np.shape(img))           
            else:
                print(paths)

        img_data = np.array(img_data) 
        #print('shape of img:',np.shape(img_data))
        return img_data

    def __init__(self, scenario, params):
        dataset = 'food101'
        self.datafile = params.data+"/meta/train_file.json"
        self.test_datafile = params.val_data
        

        if scenario == 'ni':
            num_tasks = len(params.ns_factor)
        else:
            num_tasks = params.num_tasks
        super(FOOD101, self).__init__(dataset, scenario, num_tasks, params.num_runs, params)

    def download_load(self):
        data_dict_train = unjson(self.datafile)
        data_dict_test = unjson(self.test_datafile)

        datapath_train = data_dict_train['datapath']
        self.train_data = self._get_img_from_path(datapath_train)
        self.train_label = np.asarray(data_dict_train['labels'])

        datapath_test = data_dict_test['datapath']
        self.test_data = self._get_img_from_path(datapath_test)
        self.test_label = np.asarray(data_dict_test['labels'])
    
    def setup(self):
        if self.scenario == 'ni':
            self.train_set, self.val_set, self.test_set = construct_ns_multiple_wrapper(self.train_data,
                                                                                        self.train_label,
                                                                                        self.test_data, self.test_label,
                                                                                        self.task_nums, 32,
                                                                                        self.params.val_size,
                                                                                        self.params.ns_type, self.params.ns_factor,
                                                                                        plot=self.params.plot_sample)
        elif self.scenario == 'nc':
            self.task_labels, self.cumm_tasks_overlap_details = create_task_composition_fixed(class_nums=101, num_tasks=self.task_nums, fixed_order=self.params.fix_order, external_data_flag = self.params.external_data, alpha=self.params.alpha_val, beta=self.params.beta, distribution=self.params.distribution)
            print('task labels: ', self.task_labels)
            result = []
            temp = []

            for i in range(len(self.task_labels)):
                temp.extend(self.task_labels[i])
                result.append(temp.copy())

            self.task_labels = result

            self.test_set = []
            for labels in self.task_labels:
                x_test, y_test = load_task_with_labels(self.test_data, self.test_label, labels, curr_task_details=None, cumm_overlap_details=None)
                self.test_set.append((x_test, y_test))

        else:
            raise Exception('wrong scenario')

    def new_task(self, cur_task, **kwargs):
        if self.scenario == 'ni':
            x_train, y_train = self.train_set[cur_task]
            labels = set(y_train)
        elif self.scenario == 'nc':
	    
            print('cur task: ', cur_task)
            labels = self.task_labels[cur_task]
            x_train, y_train = load_task_with_labels(self.train_data, self.train_label, labels, curr_task_details=self.cumm_tasks_overlap_details[cur_task], cumm_overlap_details=self.cumm_tasks_overlap_details[self.task_nums-1])
            #print('curr task overlap: ', self.cumm_tasks_overlap_details[cur_task].tolist())
            #print('overall task overlap: ', self.cumm_tasks_overlap_details[self.task_nums-1].tolist())
            #baseline 
            #labels = self.task_labels[cur_task]
            #x_train, y_train = load_task_with_labels_baseline(self.train_data, self.train_label, labels, curr_task_details=self.cumm_tasks_overlap_details[cur_task], cumm_overlap_details=self.cumm_tasks_overlap_details[self.task_nums-1])
        return x_train, y_train, labels

    def new_run(self, **kwargs):
        self.setup()
        return self.test_set

    def test_plot(self):
        test_ns(self.train_data[:10], self.train_label[:10], self.params.ns_type,self.params.ns_factor)
